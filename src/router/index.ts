import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Corporate from '@/views/Corporate.vue'
import Personal from '@/views/Personal.vue'
import Store from '@/views/Store.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Corporate',
    component: Corporate
  },
  {
    path: '/personal',
    name: 'Personal',
    component: Personal
  },
  {
    path: '/store',
    name: 'Store',
    component: Store
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
